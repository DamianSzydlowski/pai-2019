<!DOCTYPE html>
<head>
    <meta charset="UTF-8">

    <?php include (dirname(__DIR__).'/Common/head.php');?>
    <link rel="stylesheet" href="../../Public/css/recipie.css" type="text/css" />



    <title>IChief</title>
</head>
<body>
    <?php include(dirname(__DIR__).'./Common/navbar.php'); ?>

    <div class="container">
        <div id="recipieBg">
            <?php //foreach($recipies as $recipie): ?>
            <div id="recipie">
                <div id="headBlock">
<!--                    <img id="recipiePhoto" src="../../Public/img/uploads/images.png">-->
                    <img id="recipiePhoto" src="data:image/png;base64,<?=base64_encode($recipie->getImage())?>" onerror="this.onerror=null; this.src='../../Public/img/defaultUser.png'" alt="" >
                    <div id="titleBlock">
                        <p><?=$recipie->getTitle();?></p>
                        <div id="textBlock">
                            <p style="font-weight: bold">Time to prepare:</p>
                            <p> <?=$recipie->getTimeToPrep();?></p>
                            <p style="font-weight: bold">Rating:</p>
                            <p><?=$recipie->getStars();?></p>
                            <p style="font-weight: bold">Money:</p>
                            <p><?=$recipie->getMoneyPerServing();?>$  per serving</p>
                        </div>
                    </div>
                </div>
                <div id="recipieBody">
                    <div id="ingridientList">
                        <p> Products: </p>
                        <?php $calories = 0; $proteins = 0; $fat = 0; $carbohydrates = 0; $sodium = 0;
                        foreach ($products as $product): ?>
                        <p> * <?=$product['product']->getName() ?>  - x <?=$product['quantity']?> </p>
                        <?php  $calories += $product['product']->getEnergeticVal() * $product['quantity'];
                                      $proteins += $product['product']->getProtein() * $product['quantity'];
                                      $fat += $product['product']->getFat() * $product['quantity'];
                                      $carbohydrates += $product['product']->getCarbohydrates() * $product['quantity'];
                                      $sodium += $product['product']->getSodium() * $product['quantity'];
                        endforeach;?>
                        <hr id="divider">
                        <div id="caloriesList">
                            <p style="color: red">Calories: <?=$calories?>kcal</p>
                            <p style="color: #0f9cff">Proteins: <?=$proteins?>g</p>
                            <p style="color: #ffe12f">Fat: <?=$fat?>g</p>
                            <p style="color: #4e09ff">Carbohydrates: <?=$carbohydrates?>g</p>
                            <p style="color: #09ffb4">Sodium: <?=$sodium?>g</p>
                        </div>
                    </div>
                    <div class="overflow-auto position-static" id="recipieInstruction">
                        <p id="instructionText"><?=$recipie->getDescription();?></p>
                    </div>
                </div>
                <?php //endforeach;?>
            </div>
        </div>
    </div>
</body>
</html>
