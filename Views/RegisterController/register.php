<!DOCTYPE html>
<head>
    <meta charset="UTF-8">

    <?php include (dirname(__DIR__).'/Common/head.php');?>
    <link rel="stylesheet" href="../../Public/css/register.css" type="text/css" />

    <title>IChief</title>
</head>
<body>
    <div class="container">
        <div id="formBG">
            <div id="formContainer">
                <form id="registerForm"  method="post">
                    <p id="headerText"> Sign Up</p>
                    <div id="error">
                    </div>
                    <div class="form-group">
                        <label for="inputEmail">Email address</label>
                        <input type="email" name="email" class="form-control" id="inputEmail" aria-describedby="emailHelp" required>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail">Verify email address</label>
                        <input type="email" name="verifyEmail" class="form-control" id="inputEmailVer" aria-describedby="emailHelp" required>
                    </div>
                    <div class="form-row">
                        <div class="col form-group">
                            <label for="inputName">Name</label>
                            <input type="name" name="name" class="form-control" id="inputName" aria-describedby="emailHelp" required>
                        </div>
                        <div class="col form-group">
                            <label for="inputSurame">Surname</label>
                            <input type="surname" name="surname" class="form-control" id="inputSurame" aria-describedby="emailHelp" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword">Password</label>
                        <input type="password" name="password" class="form-control" id="inputPassword" aria-describedby="emailHelp" required>
                    </div>
                    <div class="form-group">
                        <label for="input2ndPassword">Verify password</label>
                        <input type="password" name="verifyPassword" class="form-control" id="input2ndPassword" aria-describedby="emailHelp" required>
                    </div>
                    <div class="buttonGroup">
                        <button class="" id="submitButton" type="submit">CONTINUE</button>
                        <a id="backToLogin" href="?page=login">BACK TO LOGIN</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>

<script type="text/javascript">
</script>

</html>
