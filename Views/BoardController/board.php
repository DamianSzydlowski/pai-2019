<!DOCTYPE html>
<head>
    <meta charset="UTF-8">

    <?php include (dirname(__DIR__).'/Common/head.php');?>
    <link rel="stylesheet" href="../../Public/css/board.css" type="text/css" />

    <title>IChief</title>
</head>
<body>
<div class="container">
    <?php include(dirname(__DIR__).'/Common/navbar.php'); ?>
    <div class="board">
        <?php foreach($recipies as $recipie): ?>
            <?php foreach($recipies as $recipie): ?>
            <div class="card" style="width: 18rem;">
                <img src="data:image/png;base64,<?=base64_encode($recipie->getImage())?>" onerror="this.onerror=null; this.src='../../Public/img/defaultUser.png'" class="card-img-top" alt="...">
                <div class="card-body">
                    <h4 class="card-title"><?= $recipie->getTitle() ?> </h4>
                    <p class="card-text"><?='Time to prepare: '.$recipie->getTimeToPrep() ?></p>
                    <p class="card-text"><?='Money per serving $ '.$recipie->getMoneyPerServing() ?></p>
                    <a href="?page=recipie&recipieId=<?=$recipie->getId()?>" class=" btn btn-primary stretched-link"  >Go to recipie</a>
                </div>
            </div>
            <?php endforeach ?>
        <?php endforeach ?>
    </div>
</div>
</body>

<script>
        $(function () {
            $('.goToRecipie').on('click', function () {
                console.log("Value of recipieId1: ", $('.goToRecipie').data('id'));
                console.log("Value of recipieId2: ", $(this).data('id'));
                openRecipie($(this).data('id'));
                // alert($(this).data('id'));
            });
        });

        function openRecipie(id) {
            const apiUrl = "http://localhost:8000";

            console.log("Value of recipieId3:", id);
            $.ajax({
                type: "GET",
                url: apiUrl + '/?page=recipiePanel',
                data: ({'recipieId' : id}),
                success: function(data) {
                    alert(data);
                }
            });

            // var xhr = new XMLHttpRequest();
            // xhr.open("GET", 'http://localhost:8000/?page=userPanel', true);
            // xhr.setRequestHeader('Content-Type', 'application/json');
            // xhr.send("recipieId="+id);
        }


</script>
