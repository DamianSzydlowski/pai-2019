<!DOCTYPE html>
<head>
    <meta charset="UTF-8">

    <?php include (dirname(__DIR__).'/Common/head.php');?>
    <link rel="stylesheet" href="../../Public/css/userPanel.css" type="text/css">

    <title>IChief</title>
</head>
<body>
<div class="container">
    <?php include(dirname(__DIR__).'/Common/navbar.php'); ?>
    <div class="user">
        <div id="userPlate">
            <img class="d-block rounded-circl" id="userPanelPhoto" src="data:image/jpeg;base64,<?=base64_encode($user->getPhoto())?>" onerror="this.onerror=null; this.src='../../Public/img/defaultUser.png'" alt=""/>
            <p id="userPanelName">Profile of: <?="".$user->getName()." ". $user->getSurname() ?>  </p>
        </div>
        <div id="recipiesAndStats">
            <div id="userPanelStats">
                <p>Number of Recipies: <?=sizeof($recipies)?></p>
                <p>Overall score: <?php $sum = 0; foreach ($recipies as $recipie): $sum = $sum+ $recipie->getStars(); endforeach; echo $sum / sizeof($recipies); ?></p>
            </div>
            <div id="boardWithQuickFilterAndSearch">
                <div class="btn-toolbar justify-content-between" style="margin-top: 0.4rem; margin-bottom: 0.1rem" role="toolbar" aria-label="Toolbar with button groups">
                    <div class="btn-group" role="group" aria-label="Basic example">
                        <button type="button" class="btn btn-secondary">All</button>
                        <button type="button" class="btn btn-secondary">Breakfast</button>
                        <button type="button" class="btn btn-secondary">Diner</button>
                        <button type="button" class="btn btn-secondary">Desserts</button>
                        <button type="button" class="btn btn-secondary">Snacks</button>
                    </div>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text" id="btnGroupAddon">@</div>
                        </div>
                        <input type="text" class="form-control" placeholder="@Search" aria-label="@Search" aria-describedby="btnGroupAddon">
                    </div>
                </div>
                <div class="overflow-auto position-static" id="recipieBoard">
                    <?php foreach ($recipies as $recipie): ?>
                        <div class="card mb-2" id="recipie">
                            <div class="row no-gutters">
                                <div class="col-md-3">
                                    <img src="data:image/jpeg;base64,<?=base64_encode($recipie->getImage())?>" onerror="this.onerror=null; this.src='../../Public/img/defaultUser.png'" class="card-img-top" alt="...">
                                </div>
                                <div class="col-md-8">
                                    <div class="card-body">
                                        <h4 class="card-title"><?= $recipie->getTitle() ?> </h4>
                                        <p class="card-text"><?='Time to prepare: '.$recipie->getTimeToPrep() ?></p>
                                        <p class="card-text"><?='Money per serving $ '.$recipie->getMoneyPerServing() ?></p>
                                        <a href="#" class="btn btn-primary stretched-link ">Go to recipie</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach ?>
                </div>
            </div>
        </div>
    </div>
</div>
</body>