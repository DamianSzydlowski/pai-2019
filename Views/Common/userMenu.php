<link rel="stylesheet" href="../../Public/css/userMenu.css" type="text/css" />

<div id="userMenuBg">
    <div id="userMenuBack">
        <div id="closeUserMenu" onclick="closeUserMenu()">
            <p>
                X
            </p>
        </div>
        <div id="userMenuContainer">
            <div id="userMenu">
                <button onclick="yourProfile()">
                    <p>Profile</p>
                </button>
                <button onclick="favorite()">
                    <p>Favorite</p>
                </button>
                <button onclick="yoursRecipies()">
                    <p>Yours</p>
                </button>
                <button onclick="settings()">
                    <p>Settings</p>
                </button>
                <button id="logoutButton" onclick="logout()">
                    <p>Logout</p>
                </button>
            </div>
        </div>
    </div>
</div>

