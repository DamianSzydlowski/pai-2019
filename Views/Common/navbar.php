<?php include(dirname(__DIR__) . '/Common/userMenu.php'); ?>
<?php require_once __DIR__.'./../../Repository/UserRepository.php'; ?>

<nav class="navbar navbar-expand-xl navbar-dark bg-dark sticky-top ">
<!--    <a class="navbar-brand" href="#">Navbar</a>-->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-brand logo" href="?page=board">
                    <img src="../Public/img/logo.png" srcset="../Public/img/logo.png 1x, ../Public/img/logo@2x.png 2x" alt="">
                    <span class="sr-only">(current)</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="?page=board">Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="addRecipie" href="?page=???"><i class="fas fa-plus-square"></i> Add Recipie</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Browse
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="#">Breakfast</a>
                    <a class="dropdown-item" href="#">Desserts</a>
                    <a class="dropdown-item" href="#">Diner</a>
                    <a class="dropdown-item" href="#">Snacks</a>
                </div>
            </li>
        </ul>
            <div class="nav-item">
                <button class="userButton col" onclick="openUserMenu()">
                    <div>
                        <?php if( !isset($_SESSION['id']) and !isset($_SESSION['role'])): ?>
                            <p class="userButtonText "> Sing Up </p>
                        <?php else: ?>
                            <?php $uR = new UserRepository(); $usr = $uR->getUser($_SESSION['id'])?>
                            <div></div><img class="" id="userPhoto" src="data:image/jpeg;base64,<?=base64_encode($usr->getPhoto())?>" onerror="this.onerror=null; this.src='../../Public/img/defaultUser.png'" alt=""/>
                            <p class="userButtonText d-block" > <?="".$usr->getName()." ".$usr->getSurname() ?> </p>
                        <?php endif; ?>
                    </div>
                </button>
            </div>
    </div>
</nav>

<script type="text/javascript">
    var userMenuPopUp = document.getElementById("userMenuBg");
    userMenuPopUp.focus();

    $(document).keyup(function(e) {
        if (e.key === "Escape") { // escape key maps to keycode `27`
            if ( userMenuPopUp.style.display === "block"){
                userMenuPopUp.style.display = "none";
                return false;
            }
        }
    });

    function openUserMenu() {
        <?php
        if( !isset($_SESSION['id']) and !isset($_SESSION['role'])):
            echo 'location.href = "?page=login";';
            echo 'return false;';
        else:
            echo '
            userMenuPopUp.style.display = "block";
            userMenuPopUp.focus();
            ';
        endif;
        ?>
    }

    document.getElementById("addRecipie").style.display = <?=(!isset($_SESSION['id']) and !isset($_SESSION['role'])) ? '"none"': '""';?>;
    //$('addRecipie').style.display = <?//=(!isset($_SESSION['id']) and !isset($_SESSION['role'])) ? '"none"': '""';?>//;

    function closeUserMenu() {
        userMenuPopUp.style.display = "none";
        return false;
    }


    function logout() {
        location.href = "?page=logout";
        return false;
    }

    function yourProfile() {
        <?php if( !isset($_SESSION['id']) and !isset($_SESSION['role'])): ?>
            location.disabled = true;
        <?php else: ?>
            location.href = "?page=userPanel&userId=<?=$usr->getId();?>";
        <?php endif;?>
        return false;
    }

    function yoursRecipies() {
        location.href = "?page=logout";
        return false;
    }

    function settings() {
        location.href = "?page=logout";
        return false;
    }

    function favorite() {
        location.href = "?page=logout";
        return false;
    }

</script>
