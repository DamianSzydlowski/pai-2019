<!DOCTYPE html>
<head>
    <meta charset="UTF-8">

    <?php include (dirname(__DIR__).'/Common/head.php');?>
    <link rel="stylesheet" href="../../Public/css/login.css" type="text/css" />

    <title>IChief</title>
</head>
<body>
    <?php include(dirname(__DIR__).'/Common/navbar.php'); ?>

    <div class="container">
        <div id="signUpMenuBack">
            <div id="signUpMenuBG">
                <form id="signUpMenu" action="?page=login" method="POST">
                    <div class="messages">
                        <?php
                            if(isset($messages)){
                                foreach($messages as $message) {
                                    echo $message;
                                }
                            }
                        ?>
                    </div>
                    <input name="email" type="text" placeholder="email@email.com" required>
                    <input name="password" type="password" placeholder="password" required>
                    <a href="?page=register"> Don't have accaount?</a>
                    <a href="">Don't remember password?</a>
                    <button type="submit">CONTINUE</button>
                    <button id="backToBoardButton" onclick="goToBoard()">Back to board</button>
                </form>
            </div>
        </div>
    </div>
</body>
</html>

<script type="text/javascript">
    function goToBoard() {
        location.href = "?page=board";
        return false;
    }

</script>