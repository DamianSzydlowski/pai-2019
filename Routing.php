<?php

require_once 'Controllers/BoardController.php';
require_once 'Controllers/SecurityController.php';
require_once 'Controllers/UserController.php';
require_once 'Controllers/RecipiesController.php';
require_once 'Controllers/RegisterController.php';

class Routing {
    private $routes = [];

    public function __construct()
    {
        $this->routes = [
            'board' => [
                'controller' => 'BoardController',
                'action' => 'getLatestRecipies'
            ],
            'login' => [
                'controller' => 'SecurityController',
                'action' => 'login'
            ],
            'logout' => [
                'controller' => 'SecurityController',
                'action' => 'logout'
            ],
            'userPanel' => [
                'controller' => 'UserController',
                'action' => 'getUserRecipies'
            ],
            'recipie' => [
                'controller' => 'RecipiesController',
                'action' => 'getRecipie'
            ],
            'register'=> [
                'controller' => 'RegisterController',
                'action' => 'registerUser'
            ]
        ];
    }

    public function run()
    {
        $page = isset($_GET['page']) ? $_GET['page'] : 'board';

        if (isset($this->routes[$page])) {
            $controller = $this->routes[$page]['controller'];
            $action = $this->routes[$page]['action'];

            $object = new $controller;
            $object->$action();
        }

    }
}