<?php


class ProductList
{
    private $recipieId;
    private $products = [];

    /**
     * ProductList constructor.
     * @param $recipieId
     * @param array $products
     */
    public function __construct(int $recipieId, array $products)
    {
        $this->recipieId = $recipieId;
        $this->products = $products;
    }

    /**
     * @return int
     */
    public function getRecipieId(): int
    {
        return $this->recipieId;
    }

    /**
     * @param int $recipieId
     */
    public function setRecipieId(int $recipieId): void
    {
        $this->recipieId = $recipieId;
    }

    /**
     * @return array
     */
    public function getProducts(): array
    {
        return $this->products;
    }

    /**
     * @param array $products
     */
    public function setProducts(array $products): void
    {
        $this->products = $products;
    }

}