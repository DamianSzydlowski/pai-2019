<?php

//require_once "./Repository/ImgRepository.php";

class User
{
    private $id;
    private $email;
    private $password;
    private $name;
    private $surname;
    private $photo;
    private $role = ['ROLE_USER'];

    public function __construct(
        string $email,
        string $password,
        string $name,
        string $surname,
        int $photo = null,
        int $id = null
    )
    {
        $this->email = $email;
        $this->password = $password;
        $this->name = $name;
        $this->surname = $surname;
        $this->photo = $photo;
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function getEmail(): string
    {
        return $this->email;
    }


    public function getRole(): array
    {
        return $this->role;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getSurname(): string
    {
        return $this->surname;
    }

    /**
     * @return string
     */
    public function getPhoto(): ?string
    {
        $imgRep = new ImgRepository();
        return $imgRep->getImg($this->photo);
    }

    /**
     * @param string $photo
     */
    public function setPhoto(int $photo): void
    {
        $this->photo = $photo;
    }

}