<?php

require_once "./Repository/ImgRepository.php";

class Recipie
{
    private $image;
    private $title;
    private $timeToPrep;
    private $moneyPerServing;
    private $stars;
    private $description;
    private $id;

    /**
     * Recipie constructor.
     * @param $image
     * @param $title
     * @param $timeToPrep
     * @param $moneyPerServing
     * @param $stars
     */
    public function __construct(int $image, string $title, string $timeToPrep, float $moneyPerServing, float $stars, string $description, int $id)
    {
        $this->id = $id;
        $this->title = $title;
        $this->image = $image;
        $this->timeToPrep = $timeToPrep;
        $this->moneyPerServing = $moneyPerServing;
        $this->stars = $stars;
        $this->description = $description;

    }

    /**
     * @return int
     */
    public function getId(): int
    {
//        echo $this->id.$this->image;
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getImage(): string
    {
        $imgRep = new ImgRepository();
        return $imgRep->getImg($this->image);
    }

    /**
     * @param string $image
     */
    public function setImage($image): void
    {
        $this->image = $image;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getTimeToPrep(): string
    {
        return $this->timeToPrep;
    }

    /**
     * @param integer $timeToPrep
     */
    public function setTimeToPrep($timeToPrep): void
    {
        $this->timeToPrep = $timeToPrep;
    }

    /**
     * @return integer
     */
    public function getMoneyPerServing(): float
    {
        return $this->moneyPerServing;
    }

    /**
     * @param integer $moneyPerServing
     */
    public function setMoneyPerServing($moneyPerServing): void
    {
        $this->moneyPerServing = $moneyPerServing;
    }

    /**
     * @return integer
     */
    public function getStars(): float
    {
        return $this->stars;
    }

    /**
     * @param integer $stars
     */
    public function setStars($stars): void
    {
        $this->stars = $stars;
    }

}