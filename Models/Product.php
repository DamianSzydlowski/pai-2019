<?php


class Product
{
    private $name;
    private $type;
    private $energeticVal;
    private $protein;
    private $fat;
    private $carbohydrates;
    private $sodium;
    private $id;

    /**
     * Product constructor.
     * @param $name string
     * @param $type string
     * @param $energeticVal float
     * @param $protein float
     * @param $fat float
     * @param $carbohydrates float
     * @param $sodium float
     * @param $id int
     */
    public function __construct(string $name, string $type, float $energeticVal, float $protein, float $fat, float $carbohydrates, float $sodium, int $id)
    {
        $this->name = $name;
        $this->type = $type;
        $this->energeticVal = $energeticVal;
        $this->protein = $protein;
        $this->fat = $fat;
        $this->carbohydrates = $carbohydrates;
        $this->sodium = $sodium;
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return float
     */
    public function getEnergeticVal(): float
    {
        return $this->energeticVal;
    }

    /**
     * @param float $energeticVal
     */
    public function setEnergeticVal(float $energeticVal): void
    {
        $this->energeticVal = $energeticVal;
    }

    /**
     * @return float
     */
    public function getProtein(): float
    {
        return $this->protein;
    }

    /**
     * @param float $protein
     */
    public function setProtein(float $protein): void
    {
        $this->protein = $protein;
    }

    /**
     * @return float
     */
    public function getFat(): float
    {
        return $this->fat;
    }

    /**
     * @param float $fat
     */
    public function setFat(float $fat): void
    {
        $this->fat = $fat;
    }

    /**
     * @return float
     */
    public function getCarbohydrates(): float
    {
        return $this->carbohydrates;
    }

    /**
     * @param float $carbohydrates
     */
    public function setCarbohydrates(float $carbohydrates): void
    {
        $this->carbohydrates = $carbohydrates;
    }

    /**
     * @return float
     */
    public function getSodium(): float
    {
        return $this->sodium;
    }

    /**
     * @param float $sodium
     */
    public function setSodium(float $sodium): void
    {
        $this->sodium = $sodium;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

}