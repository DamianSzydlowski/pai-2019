<?php

require_once 'AppController.php';
require_once __DIR__ . '/../Models/Recipie.php';
require_once __DIR__ . '/../Database.php';
require_once __DIR__ . '/../Repository/RecipieRepository.php';
class BoardController extends AppController
{

    public function getLatestRecipies()
    {
        $recipieRepository = new RecipieRepository();

        $data = $recipieRepository->getRecipies();

        $this->render('board', ['recipies' => $data]);
    }
}