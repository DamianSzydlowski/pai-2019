<?php

require_once 'AppController.php';
require_once __DIR__ . '/../Models/Recipie.php';
require_once __DIR__ . '/../Database.php';

class UserController extends AppController
{
    public function getUserRecipies()
    {
//        $database = new Database();
//        $database->connect()->prepare('SELECT * FROM users WHERE email != :email;');

        $recipieRepository = new RecipieRepository();
        $userRepository = new UserRepository();

        $userId = $_GET['userId'];

        $user = $userRepository->getUserById($userId);
        $data = $recipieRepository->getUserRecipies($user->getId());

        if ($user){
            $this->render('userPanel', ['recipies' => $data, 'user'=> $user]);
        }

        $url = "http://$_SERVER[HTTP_HOST]/";
        header("Location: {$url}?page=board");
        return;
    }
}