<?php

require_once "AppController.php";
require_once "Repository/RecipieRepository.php";
require_once "Repository/ProductRepository.php";

class RecipiesController extends AppController
{

    public function getRecipie()
    {
        $recipieRepository = new RecipieRepository();
        $productRepository = new ProductRepository();
        $recipieId = $_GET['recipieId'];
        $recipieData = null ;
        $productData = null;
        $recipieData = $recipieRepository->getRecipie($recipieId);
        $productData = $productRepository->getProductsFromRecipie($recipieId);

        if($recipieData && $productData){
            $this->render('recipie', ['recipie' => $recipieData,
                                        'products'=> $productData]);
        }
        else{
            $url = "http://$_SERVER[HTTP_HOST]/";
            header("Location: {$url}?page=board");
            return;
        }
    }

}