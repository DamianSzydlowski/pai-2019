<?php
require_once "AppController.php";
require_once __DIR__ . "/../Repository/UserRepository.php";

class RegisterController extends AppController
{
    function registerUser(){
        if($_POST){
            $userEmail 		= $_POST['email'];
            $userName 		= $_POST['name'];
            $userSurname 	= $_POST['surname'];
            $userPassword 	= $_POST['password'];

            $password = password_hash($userPassword, PASSWORD_BCRYPT, array('cost' => 11));

            try{
                $userRepository = new UserRepository();
                $userRepository->saveUser($userEmail, $password, $userName, $userSurname);

                $url = "http://$_SERVER[HTTP_HOST]/";
//                header("Location: {$url}?page=board");
                return;

            }
            catch (PDOException $e){
                echo $e->getMessage();
            }
        }
        else{
            $this->render('register');
        }
    }
}