$('document').ready(function()
{
    /* validation */
    $("#registerForm").validate({
        rules:
            {
                uname: {
                    required: true,
                    minlength: 3
                },
                surname: {
                    required: true,
                    minlength: 3
                },
                password: {
                    required: true,
                    minlength: 8,
                    maxlength: 30
                },
                cpassword: {
                    required: true,
                    equalTo: '#inputPassword'
                },
                email: {
                    required: true,
                    email: true
                },
                verifyEmail:{
                    required: true,
                    email: true,
                    equalTo: '#inputEmail'
                }
            },
        messages:
            {
                uname: "Enter a Valid Name",
                surname: "Enter a Valid Surname",
                password:{
                    required: "Provide a Password",
                    minlength: "Password Needs To Be Minimum of 8 Characters"
                },
                cpassword:{
                    required: "Retype Your Password",
                    equalTo: "Password Mismatch! Retype"
                },
                email: "Enter a Valid Email",
                verifyEmail: {
                    required: "Retype Your Email",
                    equalTo: "Email Mismatch! Retype"
                }

            },
        submitHandler: submitForm
    });
    /* validation */

    /* form submit */
    function submitForm()
    {
        const data = $("#registerForm").serialize();
        const apiUrl = "http://localhost:8000";

        $.ajax({

            type : 'POST',
            url  : apiUrl + '?page=register',
            data : data,
            beforeSend: function()
            {
                $("#error").fadeOut();
                $("#submitButton").html('<span class="glyphicon glyphicon-transfer"></span>   sending ...');
            },
            success :  function(data)
            {
                if(data==1){

                    $("#error").fadeIn(1000, function(){


                        $("#error").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span>   Sorry email already taken !</div>');

                        $("#submitButton").html('<span class="glyphicon glyphicon-log-in"></span>   Create Account');

                    });

                }
                else if(data=="registered")
                {

                    $("#submitButton").html('Signing Up');
                    location.replace(apiUrl + "?page=login");
                }
                else{

                    $("#error").fadeIn(1000, function(){

                        $("#error").html('<div class="alert alert-danger"><span class="glyphicon glyphicon-info-sign"></span>   '+data+' !</div>');

                        $("#submitButton").html('<span class="glyphicon glyphicon-log-in"></span>   Create Account');

                    });

                }
            }
        });
        return false;
    }
    /* form submit */

});