<?php

require_once "Repository.php";
require_once __DIR__.'//..//Models//User.php';

class UserRepository extends Repository {

    public function getUser(string $email): ?User
    {
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM users WHERE email = :email
        ');
        $stmt->bindParam(':email', $email, PDO::PARAM_STR);
        $stmt->execute();

        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        if($user == false) {
            return null;
        }

        return new User(
            $user['email'],
            $user['password'],
            $user['name'],
            $user['surname'],
            $user['userId'],
            $user['imageId']
        );
    }

    public function getUserById(int $userId): ?User
    {
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM users WHERE userId = :userId
        ');
        $stmt->bindParam(':userId', $userId, PDO::PARAM_INT);
        $stmt->execute();

        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        if($user == false) {
            return null;
        }

        return new User(
            $user['email'],
            $user['password'],
            $user['name'],
            $user['surname'],
            $user['userId'],
            $user['imageId']
        );
    }

    public function getUsers(): array {
        // $result = [];
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM users WHERE email != :email;
        ');
        $stmt->bindParam(':email', $_SESSION['id'], PDO::PARAM_STR);
        $stmt->execute();
        $users = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $users;
    }

    public function saveUser(string $email, string $password, string $name, string $surrname)
    {
        $db = $this->database->connect();
        $db->beginTransaction();
        $stmt = $db->prepare('
            SELECT * FROM users WHERE email = :email
        ');
        $stmt->bindParam(":email",$email);
        $stmt->execute();
        $res = $stmt->fetchAll();

        if(!$res){
            $stmt = $db->prepare('
                INSERT INTO users(email, password, name, surname) VALUES(:email, :pass, :uname, :surname)
            ');
            $stmt->bindParam(":email",$email);
            $stmt->bindParam(":pass",$password);
            $stmt->bindParam(":uname",$name);
            $stmt->bindParam(":surname",$surrname);

            if($stmt->execute())
            {
                echo "registered";
                $db->commit();
            }
            else
            {
                echo "Query could not execute !";
                $db->rollBack();
            }
        }
        else{
            echo "1"; //  not available
            $db->rollBack();
        }


    }

}