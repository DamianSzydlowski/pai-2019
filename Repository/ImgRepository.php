<?php

require_once "Repository.php";
class ImgRepository extends Repository
{
    public function getImg(int $imageId): ?string
    {
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM image WHERE imageId = :imageId
        ');
        $stmt->bindParam(':imageId', $imageId, PDO::PARAM_INT );
        $stmt->execute();

        $img = $stmt->fetch(PDO::FETCH_ASSOC);


        if($img == false) {
            return null;
        }

        return $img['photo'];
    }
}