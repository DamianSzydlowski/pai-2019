<?php

require_once "Repository.php";
require_once "Models/Recipie.php";

class RecipieRepository extends Repository
{
    public function getRecipie(int $recipieId): ?Recipie
    {
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM recipie WHERE recipieId = :recipieId
        ');
        $stmt->bindParam(':recipieId', $recipieId, PDO::PARAM_INT);
        $stmt->execute();

        $recipie = $stmt->fetch(PDO::FETCH_ASSOC);

        if($recipie == false) {
            return null;
        }

        return new Recipie(
            $recipie['imageId'],
            $recipie['name'],
            $recipie['prepTime'],
            $recipie['perServing'],
            $recipie['grade'],
            $recipie['description'],
            $recipie['imageId']
        );
    }

    public function getUserRecipies(int $userId): ?array
    {
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM userrecipies, recipie WHERE userrecipies.userId = :userId and recipie.recipieId = userrecipies.recipieId;
        ');
        $stmt->bindParam(':userId', $userId, PDO::PARAM_INT);
        $stmt->execute();

        $recipies = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($recipies as $recipie):
           $result[]= new Recipie($recipie['imageId'],
                $recipie['name'],
                $recipie['prepTime'],
                $recipie['perServing'],
                $recipie['grade'],
                $recipie['description'],
                $recipie['recipieId']);
        endforeach;

        return $result;
    }

    public function getRecipies(): ?array
    {
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM recipie;
        ');
        $stmt->execute();

        $recipies = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if ($recipies == null){
            return null;
        }

        $data = array();

        foreach ($recipies as $recipie):
            array_push($data, new Recipie($recipie['imageId'],
                $recipie['name'],
                $recipie['prepTime'],
                $recipie['perServing'],
                $recipie['grade'],
                $recipie['description'],
                $recipie['recipieId']));
        endforeach;

        return $data;

    }

}