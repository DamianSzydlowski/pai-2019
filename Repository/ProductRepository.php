<?php

require_once "Repository.php";
require_once "Models/Product.php";

class ProductRepository extends Repository
{
    public function getProduct(int $productId){
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM product WHERE productId = :productId
        ');
        $stmt->bindParam(':productId', $productId, PDO::PARAM_INT);
        $stmt->execute();

        $product = $stmt->fetch(PDO::FETCH_ASSOC);

        if($product == false) {
            return null;
        }

        return new Product(
            $product['Name'],
            $product['Type'],
            $product['Energetic'],
            $product['Protein'],
            $product['Fat'],
            $product['Carbohydrates'],
            $product['Sodium'],
            $product['productId'],
        );
    }

    public function getProductsFromRecipie(int $recipieId): ?array
    {
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM productlist, product WHERE productlist.recipieId = :recipieId and productlist.productId = product.productId;
        ');
        $stmt->bindParam(':recipieId', $recipieId, PDO::PARAM_INT);
        $stmt->execute();
        $productsRepo = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $products = [];

        foreach ($productsRepo as $product):
            array_push($products, ['product' => new Product(
                $product['Name'],
                $product['Type'],
                $product['Energetic'],
                $product['Protein'],
                $product['Fat'],
                $product['Carbohydrates'],
                $product['Sodium'],
                $product['productId']
            ), 'quantity' => $product['quantity']]);
        endforeach;

        return $products;
    }

    public function getUserProducts(int $userId){
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM userproducts, product WHERE userproducts.userId = :userId and userproducts.productId = product.productId;
        ');
        $stmt->bindParam(':userId', $userId, PDO::PARAM_INT);
        $stmt->execute();
        $products = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $products;
    }



}